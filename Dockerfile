FROM node:12-alpine

LABEL maintainer="Leonardo Peixoto"

ENV NODE_ENV=development

WORKDIR /var/www

ADD package.json .
ADD package-lock.json .

RUN npm i

COPY . .

CMD ["npm", "run", "dev"]