const sinon = require('sinon');
const fakeUsers = require('./fakeUsers');
const UserService = require('../../../src/services/UserService');

async function fakeList(order = false) {
  const users = fakeUsers;

  if (order) {
    return users.sort((user, nextUser) => user.name.localeCompare(nextUser.name));
  }

  return users;
}

describe('UserService', () => {
  before((done) => {
    sinon.replace(UserService, 'list', fakeList);
    done();
  });

  after(() => {
    sinon.restore();
  });

  describe('Deve listar todos os websites', () => {
    it('Deve retornar um array somente com os websites dos usuários', async () => {
      const websites = await UserService.websites();
      
      websites.forEach(website => assert.hasAllKeys(website, ['website']));
    })
  });

  describe('Deve listar todas as informações do usuário', () => {
    it('Deve retornar um array somente com (name, email e company) dos usuários', async () => {
      const infoUsers = await UserService.infoUsers();

      infoUsers.forEach(info => assert.hasAllKeys(info, ['name', 'email', 'company']));
    });

    it('Deve estar em ordem alfabética', async() => {
      const infoUsers = await UserService.infoUsers();
      const infoUsersOrder = require('./infoUsersOrder');

      assert.sameDeepOrderedMembers(infoUsers, infoUsersOrder);
    });
  });

  describe('Deve listar os usuários de acordo com um endereço passado no filtro', () => {
    it('Deve listar todos os usuários que no endereço contem a palavra suite', async () => {
      const users = await UserService.searchAddress('suite');

      users.forEach(({ address }) => assert.match(`${address.street} ${address.suite} ${address.city}`, /suite/i));
    });
  });
});
