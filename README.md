# API de Usuários

Api responsável por consumir um serviço que lista os usuários cadastrados. Guardando o log das interações
do usuário com a api no Elasticseach.

## Dependências
+ Node v.12
+ Elasticsearch v.7.1
+ Docker v.18
+ Docker compose v1.24

## Estilo de Código

O projeto conta com eslint padrão air-bnb. A cada commit é feito uma verificação
se código está seguindo o padrão estabelicido. Caso contrário haverá um erro mostra, o respectivo arquivo e
qual regra eslint está sendo quebrada. Alguns erros são corrigidos automáticamente.

## Setup
Após instalar as dependâncias listadas a cima, poderemos executar o projeto executando um comando no bash: 
```sh
npm start
```
Esse comando ira criar uma instância da aplicação no pm2. 

Em ambiente de desenvolvimento podemos usar o comando:
```sh
npm run dev
```
ou caso prefira subir um container com as dependância prontas para uso, podemos executar: 
```sh
npm run dev-docker
```

**Obs.:** Caso seu usuário não tenha permissão para executar o docker execute o comando como sudo:
```sh
sudo npm run dev-docker
```

## Rotas

+ GET /api/users - lista todos os usuários cadastrados
+ GET /api/users/info - lista apenas o nome, o email e a empresa dos usuários cadastrados em ordem alfabética 
+ GET /api/users/websites - lista apenas o website dos usuários cadastrados
+ GET /api/users/search/address/:filter - lista apenas os usuários que tenham o endereço correspondendo ao parâmetro passado em :filter

## Test

Para a realização dos testes unitários basta executar:

```sh
npm run test
```