const { join } = require('path');

const pathFilesConfig = join('..', 'config');

/**
 * Retorna o valor de uma terminada configuração
 * @param {string} key - nome do arquivo e chave.
 * Ex.: file.key
 * @returns {string} valor do parâmetro de configuração
 */
function get(key) {
  const [fileName, keyConfig] = key.split('.');

  const path = join(pathFilesConfig, fileName);

  // eslint-disable-next-line
  const configObject = require(path);

  return configObject[keyConfig];
}

module.exports = {
  get,
};
