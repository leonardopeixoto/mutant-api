const client = require('./els');

module.exports = (req, res, next) => {
  const request = {
    env: process.env.NODE_ENV || 'development',
    request: {
      body: req.body,
      params: req.params,
      method: req.method,
      ip: req.ip,
      path: req.originalUrl,
    },
    '@timestamp': (new Date()).toISOString(),
  };

  client
    .index({
      index: 'log',
      type: 'info',
      body: request,
    })
    .then(() => next())
    .catch(error => res.status(500).json(error));
};
