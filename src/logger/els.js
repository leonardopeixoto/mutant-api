const { Client } = require('es7');
const config = require('../helpers/config');

const client = new Client({
  node: config.get('elasticsearch.host'),
});


module.exports = client;
