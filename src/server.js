const { Server } = require('http');
const config = require('./helpers/config');
const app = require('./app');

const port = config.get('server.port');
const httpServer = Server(app);

httpServer.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`listening on ${port}`);
});
