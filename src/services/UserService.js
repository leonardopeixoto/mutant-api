const config = require('../helpers/config');
const axios = require('../helpers/axios')(config.get('services.jsonplaceholder'));

/**
 * Retorna uma lista de usuários
 * @param {boolean} order
 * @returns {array}
 */
async function list(order = false) {
  const { data: users } = await axios.get('/users');

  if (order) {
    return users.sort((user, nextUser) => user.name.localeCompare(nextUser.name));
  }

  return users;
}

/**
 * Retorna uma lista de todos os websites dos usuários cadastrados
 * @returns {array}
 */
async function websites() {
  const users = await this.list();

  return users.map(({ website }) => ({ website }));
}

/**
 * Retorna uma lista contendo nome, email e empresa em que trabalha
 * dos usuários cadastrados
 * @returns {array}
 */
async function infoUsers() {
  const users = await this.list(true);

  return users.map(({ name, email, company }) => ({ name, email, company }));
}

/**
 * Filtra os usuários cadastrados pelo endereço
 * @param {string} addressFilter
 * @returns {array}
 */
async function searchAddress(addressFilter = '') {
  const users = await this.list();

  const filterRegex = new RegExp(`${addressFilter}`, 'i');

  return users.filter(({ address }) => filterRegex.test(`${address.street} ${address.suite} ${address.city}`));
}

module.exports = {
  list,
  websites,
  infoUsers,
  searchAddress,
};
