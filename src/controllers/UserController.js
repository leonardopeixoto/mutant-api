const UserService = require('../services/UserService');

async function index(req, res) {
  const users = await UserService.list();

  res.json(users);
}

async function listWebsites(req, res) {
  const websites = await UserService.websites();

  res.json(websites);
}

async function info(req, res) {
  const infoUsers = await UserService.infoUsers();

  res.json(infoUsers);
}

async function searchAddress(req, res) {
  const { filter } = req.params;

  const users = await UserService.searchAddress(filter);

  res.json(users);
}

module.exports = {
  index,
  listWebsites,
  info,
  searchAddress,
};
