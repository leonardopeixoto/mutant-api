const helmet = require('helmet');
const express = require('express');
const cors = require('cors');
const routes = require('./routes');
const logger = require('./logger');

const app = express();

//
// Register Node.js middleware
// -----------------------------------------------------------------------------
app.use(helmet());
app.use(cors());

app.use(logger);
routes(app);

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.send(res.locals.error);
});


module.exports = app;
