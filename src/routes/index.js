const usersRoutes = require('./users');

module.exports = (app) => {
  app.use('/api', usersRoutes);
};
