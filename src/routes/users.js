const express = require('express');
const asynMiddleware = require('../helpers/asynMiddleware');
const UserController = require('../controllers/UserController');

const routerController = express.Router();
const routerActions = express.Router();

routerActions
  .get('/', asynMiddleware(UserController.index));

routerActions
  .get('/info', asynMiddleware(UserController.info));

routerActions
  .get('/websites', asynMiddleware(UserController.listWebsites));

routerActions
  .get('/search/address/:filter', asynMiddleware(UserController.searchAddress));

module.exports = routerController.use('/users', routerActions);
